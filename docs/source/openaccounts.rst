ดึงข้อมูลการเดิมพันของผู่เล่น (VPSOpenAccounts)
======================================================================
ดึงข้อมูลการเดิมพันของผู่เล่น (current betting information, unsettled betting slip)

current betting
***********************

Input content
-------------

================ ================ ================ ================ ======================================================= 
parameter name   type             length           required         description
================ ================ ================ ================ =======================================================
RequestID        string           32               yes              เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้ง
                                                                    รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
BucketID         string           1-20             yes              BucketID
ServerName       string           1-50             no               Game server name (please refer to table 8)
                                                                    รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
GameType         int              1-4              no               Game types (please refer to table 6)
MemberID         string           1-20             no               รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
ProviderID       string           `-`              no               Game vendor ID, default is Royal
StartTime        string           `-`              no               ตัวอย่าง 2018-01-01 13:00:00
                                                                    รูปแบบ Y-m-d H:i:s
EndTime          string           `-`              no               ตัวอย่าง 2018-01-01 13:00:00
                                                                    รูปแบบ Y-m-d H:i:s
================ ================ ================ ================ =======================================================

Output content
--------------

=========================================================== ================ ================ ======================================================= 
field                                                       type             length           description
=========================================================== ================ ================ =======================================================
RequestID                                                   string           32               เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้งค่าจะต้องตรงกับ input ที่ส่งไป
                                                                                              รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
ErrorCode                                                   string           1-4              สถานะ 0 สำเร็จ อื่นๆ มีข้อผิดพลาด
ErrorMessage                                                string                            ข้อความ Error
List                                                        array                             อเรย์ของข้อมูล
List.BucketID                                               string           1-20             BucketID
List.MemberList                                             array            `-`              
List.MemberList.Member                                      string           1-20             บัญชีผมูเล่น
List.MemberList.BetDetail                                   array            `-`              
List.MemberList.BetDetail.GameList                          array            `-`  
List.MemberList.BetDetail.GameList.GameType                 string           `-`              Game types (please refer to table 6)
List.MemberList.BetDetail.GameList.Game                     array            `-`              Game table list
List.MemberList.BetDetail.GameList.Game.ID                  string           1-20             Unsettled note order serial number (different from the note number output by the account pulling method)
                                                                                              Format: Number
List.MemberList.BetDetail.GameList.Game.BetRequestID        string           32               Bet Request ID รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
List.MemberList.BetDetail.GameList.Game.CurrencyID          string           1-10             สกุลเงิน (please refer to table 11)
List.MemberList.BetDetail.GameList.Game.GameProviderID      string           1-20             Game Provider ID
List.MemberList.BetDetail.GameList.Game.ServerID            string           1-100            Game server ID (Please refer to table 8)
List.MemberList.BetDetail.GameList.Game.ServerName          string           1-20             Game server name
List.MemberList.BetDetail.GameList.Game.NoRun               string           1-20             
List.MemberList.BetDetail.GameList.Game.NoActive            string           1-20             
List.MemberList.BetDetail.GameList.Game.StakeID             string           1-20             
List.MemberList.BetDetail.GameList.Game.StakeName           string           1-50 
List.MemberList.BetDetail.GameList.Game.Odds                string           4-14             Odds
                                                                                              รูปแบบ: เลขฐาน10 ทศนิยม 2 ตำแหน่ง
List.MemberList.BetDetail.GameList.Game.BetScore            string           6-16             Bet Amount (Inventory)                                                                                              
                                                                                              รูปแบบ: เลขฐาน10 ทศนิยม 4 ตำแหน่ง
List.MemberList.BetDetail.GameList.Game.BetStatus           string           1-4              Bet status (Please refer to table 10)
List.MemberList.BetDetail.GameList.Game.ClientIP            string           1-50             IP รูปแบบ: IPV4, IPV6
List.MemberList.BetDetail.GameList.Game.ClientType          string           1-4              Client type ID (1: web version, 2: download version, 3: mobile version)
List.MemberList.BetDetail.GameList.Game.SubClientType       string           `-`              Not currently used
List.MemberList.BetDetail.GameList.Game.BetTime             string           1-50             Bet Time รูปแบบ: Y-m-d H:i:s
Page                                                        int
PageRows                                                    int
TotalRows                                                   int
TotalPage                                                   int
=========================================================== ================ ================ =======================================================

Response status code and description
------------------------------------

================ ======================================================= 
ErrorCode        description
================ =======================================================
0                successful
V200             Database response failed
V405             Transfer call API failed
================ =======================================================

Example Input
-------------
.. code-block:: JSON
    :linenos:

    {
        "RequestID": "591524d801e62591524d80779a077799",
        "BucketID": "h3roy"
    }

Example Output
---------------
.. code-block:: JSON
    :linenos:

    {
        "RequestID": "ed9553dcd87746db9a71e3c4d55d61be",
        "List": [
            {
            "BucketID": "h3roy",
            "MemberList": [
                {
                "Member": "THRONinJaAoF",
                "BetDetail": {
                    "GameList": [
                    {
                        "GameType": "1",
                        "Game": [
                        {
                            "ID": "44994192",
                            "BetRequestID": "4fcf461d56c24118a3466c3c87923f6e",
                            "CurrencyID": "THB",
                            "GameProviderID": "Royal",
                            "ServerID": "0604250006",
                            "ServerName": "BaccB",
                            "NoRun": "020823009",
                            "NoActive": "0431",
                            "StakeID": "06042500060001",
                            "StakeName": "Banker",
                            "Odds": "1.95",
                            "BetScore": "50.0000",
                            "BetStatus": "1",
                            "ClientIP": "184.22.15.151",
                            "ClientType": "3",
                            "BetTime": "2019-08-23 20:16:47.000",
                            "SubClientType": "s"
                        }
                        ]
                    }
                    ]
                }
                },
                {
                "Member": "THROMingsit",
                "BetDetail": {
                    "GameList": [
                    {
                        "GameType": "1",
                        "Game": [
                        {
                            "ID": "44994185",
                            "BetRequestID": "2b67 de9f84c24b77b84db36c9bd564ff",
                            "CurrencyID": "THB",
                            "GameProviderID": "Royal",
                            "ServerID": "0604250002",
                            "ServerName": "BaccA",
                            "NoRun": "010823010",
                            "NoActive": "0434",
                            "StakeID": "06042500020002",
                            "StakeName": "Player",
                            "Odds": "2.00",
                            "BetScore": "1400.0000",
                            "BetStatus": "1",
                            "ClientIP": "223.24.166.117",
                            "ClientType": "3",
                            "SubClientType": "s",
                            "BetTime": "2019-08-23 20:16:40.000"
                        }
                        ]
                    }
                    ]
                }
                }
            ]
            }
        ],
        "ErrorCode": "0",
        "ErrorMessage": "成功",
        "Page": null,
        "PageRow": 9999,
        "TotalRows": 2,
        "TotalPage": 0
    }

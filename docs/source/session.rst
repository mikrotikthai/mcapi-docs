session key สำหรับเข้าเกมส์ (VPSGetMemberSessionKey)
========================================================================

ก่อนที่จะเข้าสู่เกม สร้าง session key เพื่อ เข้าสู่เกม(entry page)

สร้าง session key
**************************

Input content
-------------

================ ================ ================ ================ ======================================================= 
parameter name   type             length           required         description
================ ================ ================ ================ =======================================================
RequestID        string           32               yes              เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้ง
                                                                    รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
BucketID         string           1-20             yes              BucketID
MemberID         string           1-20             yes              บัญชีสมาชิก
                                                                    รูปแบบ: อันเดอร์สกอร์, เครื่องหมายลบ, อังกฤษ, ตัวเลข
Password         string           4-32             yes              รหัสผ่านสมาชิก
                                                                    รูปแบบ: อันเดอร์สกอร์, เครื่องหมายลบ, อังกฤษ, ตัวเลข
GameType         int              `-`              yes              Game types (please refer to table 6)
ServerName       string           1-50             yes              Game server name (please refer to table 1 )
                                                                    ตัวอย่างเช่น  BaccI ("Bacc"+"I")
                                                                    รูปแบบ: GameItem + Game Server 
Lang             int              `-`              no               ภาษาของเกมเมื่อเข้าเล่นเกม (please refer to the attached table 9)
                                                                    (Not required, default is 2 Traditional Chinese)
IsDemo           int              `-`              no               Whether to try (1: Yes, 0: No, preset 0)
                                                                    (Please refer to table VIII. Not all games are available for trial)
                                                                    (It is recommended to use the RG game to try the entry page without going through the account)
================ ================ ================ ================ =======================================================

Output content
--------------

=============================== ================ ================ ======================================================= 
field                           type             length           description
=============================== ================ ================ =======================================================
RequestID                       string           32               เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้งค่าจะต้องตรงกับ input ที่ส่งไป
                                                                  รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
ErrorCode                       string           1-4              สถานะ 0 สำเร็จ อื่นๆ มีข้อผิดพลาด
ErrorMessage                    string                            ข้อความ Error
SessionKey                      string                            SessionKey สำหรับเข้าเกม
                                                                  (SessionKey is one-time, it will be invalid after entering the entry page and need to be regenerated)
                                                                  รูปแบบ: อันเดอร์สกอร์, เครื่องหมายลบ, อังกฤษ, ตัวเลข              
=============================== ================ ================ =======================================================

Response status code and description
------------------------------------

================ ======================================================= 
ErrorCode        description
================ =======================================================
0                successful
V001             ข้อมูลมีรูปแบบที่ไม่ถูกต้อง
V002             Parameter required
V205             ข้อมูลนั้นไม่มีอยู่
V200             ฐานข้อมูลไม่สามารถตอบสนอง
V405             ข้อผิดพลาดในการโอนสาย API
================ =======================================================

Example Input - Royal Live Hundred A Table
-------------------------------------------
.. code-block:: JSON
    :linenos:

    {
        "RequestID": "665d5e6c25ad439b98c2d1e16817f967",
        "BucketID": "h1",
        "MemberID": "sova009",
        "Password": "cf79ae6addba60ad018347359bd144d2",
        "GameType": 1,
        "ServerName": "BaccA"
    }

Example Input - Royal Real Hall
-------------------------------------------
.. code-block:: JSON
    :linenos:

    {
        "RequestID": "665d5e6c25ad439b98c2d1e16817f967",
        "BucketID": "h1",
        "MemberID": "sova009" ,
        "Password": "cf79ae6addba60ad018347359bd144d2",
        "GameType": 1,
        "ServerName": "lobby"
    }

Example Input - Royal Net Electric One Hundred 1 Table
--------------------------------------------------------------------
.. code-block:: JSON
    :linenos:

    {
        "RequestID": "665d5e6c25ad439b98c2d1e16817f967",
        "BucketID": "h1",
        "MemberID": "sova009",
        "Password": "cf79ae6addba60ad018347359bd144d2",
        "GameType": 1,
        "ServerName": "pbw_bacc1"
    }

Example Input - RRoyal Nets Electric Hall
----------------------------------------------------
.. code-block:: JSON
    :linenos:

    {
        "RequestID": "665d5e6c25ad439b98c2d1e16817f967",
        "BucketID": "h1",
        "MemberID": "sova009",
        "Password": "cf79ae6addba60ad018347359bd144d2",
        "GameType": 1,
        "ServerName": "pbw_lobby"
    }

Example Input - PNG Lucky Lucky Cat
-------------------------------------------
.. code-block:: JSON
    :linenos:

    {
        "RequestID": "665d5e6c25ad439b98c2d1e16817f967",
        "BucketID": "h1",
        "MemberID": "sova009",
        "Password": "cf79ae6addba60ad018347359bd144d2",
        "GameType": 3,
        "ServerName": "png_bigwincat"
    }


Example Output
---------------
.. code-block:: JSON
    :linenos:

    {
        "RequestID": "d593fbb2716f4a15b598ec3234988887",
        "ErrorCode":"0",
        "ErrorMessage":"Success",
        "SessionKey": "6531DA0C86A3D1095A6700003E57B31D"
    }
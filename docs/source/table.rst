table 1 : game
===============

===================== =============== =============== ===============
Game maker            Game Name       GameItem        Game Server
===================== =============== =============== ===============
Royal live game       Baccarat        Bacc            I, J, Y, Z
Royal live game       Roulette        LunPan          C
Royal live game       Hi Low          ShaiZi          C
Royal live game(sexy) Baccarat        Bacc            P, Q, R, S
Royal live game(sexy) LongHu          LongHu          K, L
===================== =============== =============== ===============

table 6 : GameType
==================

=================== ========================
Code                Name
=================== ========================
1                   Live video
2                   Lottery games
3                   Electronic games
4                   sports game
5                   Live battle
6                   financial product
7                   Teleport games
=================== ========================


table 7 : Game Vendors ProviderID
==================================

=================== =============================== ===================
ProviderID          ProviderName                    GameType
=================== =============================== ===================
Royal               Royal Real                      1
PhoneBetWeb         Royal Nets Electricity Live One 1
thai                Thai color                      2
png                 Play'n Go                       3
PhoneBet            Royal Telecom                   7
=================== =============================== ===================

table 9 : Language
==================================

=================== =============================== ===================
Language name       Language Code                   Code
=================== =============================== ===================
Simplified Chinese  zh-cn                           1
traditional Chinese zh-tw                           2
English             en-us                           3
Thai                th-th                           4
Korean              ko-kr                           5
Burmese             my-mm                           6
Indonesian          id-id                           7
=================== =============================== ===================

table 10 : BetStatus
===============================

=================== ========================
Code                Description
=================== ========================
1                   Bet (Unsettled)
3                   Authorities cancel
4                   Normal statistics
5                   Recount
6                   Cancel afterwards
-1                  Bet response failed: (An explicit response with an error code.)
-2                  No response to bet: (the server is abnormal when connection is getting worse)
7                   Clear note
=================== ========================

table 11 : Currency
===============================

=================== ========================
Code                CurrencyName
=================== ========================
NT                  Taiwan dollar
RMB                 RMB
THB                 Thai Bath
IDR                 Indonesian rupee
USD                 USD
VND                 Vietnamese Dong
MYR                 Malay
MMK                 Burmese kyat
=================== ========================



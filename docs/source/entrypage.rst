ทางเช้าเกมส์ (RG game entry page)
===========================================

สำหรับเข้าสู่เกม โดยใช้ SessionKey ที่ได้รับมาจาก API method VPSGetMemberSessionKey หาก SessionKey ถูกต้องจะ redirect เข้าสู่เกม

เช้าเกมส์
**************************

Input content
-------------

================ ================ ======================================================= 
parameter name   required         description
================ ================ =======================================================
SessionKey       yes              SessionKey ที่ได้มาจาก VPSGetMemberSessionKey
nbkey            no               Restriction template number (if multiple sets of restricted bets are used in the bucket, it must be brought in, only for royal real people)
================ ================ =======================================================

Reference input example
--------------------------

    - Into the table
        {Entrance URL}/Entrance?SessionKey=2C3CB4842FB3FDC8EAC600007DA15E99

    - Enter the table (specify limit template number 3, only for real people)
        {Entrance URL}/Entrance?SessionKey=2C3CB4842FB3FDC8EAC600007DA15E99&nbkey=3
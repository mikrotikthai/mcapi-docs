ผลของเกม (VPSOpenRecord)
==============================
ดึงข้อมูลผลของเกม สามารถดึงผลของเกมได้ด้วยการระบุ ช่วงเวลาที่ต้องการ(สำหรับเกมสดเท่านั้น)

ดึงข้อมูลผลของเกม
***********************

Input content
-------------

================ ================ ================ ================ ======================================================= 
parameter name   type             length           required         description
================ ================ ================ ================ =======================================================
RequestID        string           32               yes              เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้ง
                                                                    รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
BucketID         string           1-20             yes              BucketID
ServerName       string           1-50             yes              Game server name (please refer to table 8)
                                                                    รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
Date             string           `-`              yes              วันทีต้องการดึงข้อมูล เช่น 2015-03-26
                                                                    รูปแบบ: Y-m-d
NoRunStart       string           1-20             no               `-` 
NoActiveStart    string           1-20             no               `-`
NoRunEnd         string           1-20             no               `-`
NoActiveEnd      string           1-20             no               `-`
Page             int              `-`              no               หน้า ค่าเริ่มต้น: 1
PageRows         int              `-`              no               จำนวนแถวต่อหน้า ค่าเริ่มต้น: ทั้งหมด
================ ================ ================ ================ =======================================================

Output content
--------------

=========================== ================ ================ ======================================================= 
field                       type             length           description
=========================== ================ ================ =======================================================
RequestID                   string           32               เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้งค่าจะต้องตรงกับ input ที่ส่งไป
                                                              รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
ErrorCode                   string           1-4              สถานะ 0 สำเร็จ อื่นๆ มีข้อผิดพลาด
ErrorMessage                string                            ข้อความ Error
List                        array                             อเรย์ของข้อมูล
List.GameID                 string           1-50             game item ID
                                                              รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
List.Win                    array            1-20             ผลแพ้ชนะ
List.OpenTime               string           `-`              เวลาเปิดทำการ เช่น 2015-03-26 14:00:00
                                                              รูปแบบ:Y-m-d H:i:s
List.LastModifyTime         string           `-`              แก้ไขล่าสุด
                                                              รูปแบบ:Y-m-d H:i:s
List.NoRun                  string           1-20             รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
List.NoActive               string           1-20             
List.OpenRecord             array            `-`              
List.OpenRecord.{i}.Index   string           1-4              
List.OpenRecord.{i}.Result  string           1-20
List.Comment                string           `-`
List.ViewURL                string           `-`
RowCount                    int                               จำนวนแถว
Page                        int                               หน้า
PageRows                    int                               จำนวนแถวต่อหน้า
TotalRows                   int                               จำนวนแถวทั้งหมด
TotalPage                   int                               จำนวนหน้าทั้งหมด
=========================== ================ ================ =======================================================

Response status code and description
------------------------------------

================ ======================================================= 
ErrorCode        description
================ =======================================================
0                successful
V001             Missing required parameters
================ =======================================================

Example Input
-------------
.. code-block:: JSON
    :linenos:

    {
        "RequestID": "9888867789a4551e82f1456110060000",
        "BucketID": "h3RealTHSiteA",
        "ServerName": "BaccB",
        "Date": "2017-01-17",
        "Page": 1,
        "PageRows": 5
    }


Example Output
---------------
.. code-block:: JSON
    :linenos:

    {
        "RequestID": "9888867789a4551e82f1456110060000",
        "ErrorCode": "0",
        "ErrorMessage": "成功",
        "List": [{
                "GameID": "Bacc",
                "Win": [
                    "Banker"
                ],
                "OpenTime": "2016-01-18 11:06:55",
                "LastModifyTime": null,
                "NoRun": "020117023",
                "NoActive": "1190",
                "OpenRecord": [{
                        "Index": "1",
                        "Result": "17"
                    },{
                        "Index": "2",
                        "Result": "3Q"
                    },{
                        "Index": "3",
                        "Result": "25"
                    },{
                        "Index": "4",
                        "Result": "15"
                    },{
                        "Index": "5",
                        "Result": "1A"
                    }],
                "Comment": "",
                "ViewURL": ""
            },{
                "GameID": "Bacc",
                "Win": [
                    "Player"
                ],
                "OpenTime": "2016-01-18 11:05:58",
                "LastModifyTime": null,
                "NoRun": "020117023",
                "NoActive": "1189",
                "OpenRecord": [{
                        "Index": "1",
                        "Result": "18"
                    },{
                        "Index": "2",
                        "Result": "15"
                    },{
                        "Index": "3",
                        "Result": "41"
                    },{
                        "Index": "4",
                        "Result": "26"
                    }],
                "Comment": "",
                "ViewURL": ""
            },{
                "GameID": "Bacc",
                "Win": [
                    "Banker"
                ],
                "OpenTime": "2016-01-18 11:05:10",
                "LastModifyTime": null,
                "NoRun": "020117023",
                "NoActive": "1188",
                "OpenRecord": [{
                        "Index": "1",
                        "Result": "12"
                    },{
                        "Index": "2",
                        "Result": "3K"
                    },{
                        "Index": "3",
                        "Result": "4A"
                    },{
                        "Index": "4",
                        "Result": "11"
                    },{
                        "Index": "5",
                        "Result": "21"
                    },{
                        "Index": "6",
                        "Result": "14"
                    }],
                "Comment": "",
                "ViewURL": ""
            }],
        "RowCount": 3,
        "Page": 1,
        "PageRows": 3,
        "TotalRows": 1250,
        "TotalPage": 417
    }


Mclub API documentation!
=================================

    - This integration API uses UTF-8 JSON as the main output content.
    - The calling methods available for this set of integration APIs are: *POST*.
    - The time of this game system uses GMT + 8 time zone without daylight saving time.
    - This game system uses 12 noon as the accounting interval, that is, "accounting information on 5/1" represents the accounting information between 5/1 12:00 PM and 5/2 12:00 PM
    - Please refer to the following for detailed API specifications.

information exchange confidentiality instructions
----------------------------------------------------
    - สิ่งที่จำเป็นสำหรับการเชื่อต่อ API 
        - account [id] 
        - password [token] 
        - server IP ที่ใช้ส่ง request มายัง API

============================ =========================== =======================================================
name                         form                        Description
============================ =========================== =======================================================
Authorization                HTTP Header                 required for HTTP Basic Authentication (id and token)
Content-Type                 HTTP Header                 application/json
X-RG-Ver                     HTTP Header                 1
X-RG-Out                     HTTP Header                 JSON
X-RG-Time                    HTTP Header                 Current Unix Timestamp
X-RG-Hash                    HTTP Header                 md5(token + call + data + time)
============================ =========================== =======================================================

============================ =========================== =======================================================
name                         form                        Description
============================ =========================== =======================================================
call                         Body(form-data)             API name (เช่น VPSWMember )
data                         Body(form-data)             JSON Data (Input content)
============================ =========================== =======================================================

.. toctree::
    :maxdepth: 2
    :caption: API Resources:
    
    member
    balance
    fund
    
    session
    entrypage
    stake
    openrecord
    openaccounts
    memberlimit
    table


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

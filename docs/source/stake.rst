ประวัติการเดิมพัน (VPSStakeDetail2)
==============================================

    - Version
        X-RG-VER=1


ประวัติการเดิมพัน
***********************

Input content
-------------

================ ================ ================ ================ ======================================================= 
parameter name   type             length           required         description
================ ================ ================ ================ =======================================================
RequestID        string           32               yes              เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้ง
                                                                    รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
BucketID         string           1-20             yes              BucketID
MaxID            int              `-`              yes              Result.ID เริ่มหลังจากค่า MaxID
GameType         int              1-4              no               Game types (please refer to table 7)
ProviderID       string           1-20             no               Game providers (please refer to table 7)
================ ================ ================ ================ =======================================================

Output content
--------------

=========================== ================ ================ ======================================================= 
field                       type             length           description
=========================== ================ ================ =======================================================
RequestID                   string           32               เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้งค่าจะต้องตรงกับ input ที่ส่งไป
                                                              รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
ErrorCode                   string           1-4              สถานะ 0 สำเร็จ อื่นๆ มีข้อผิดพลาด
ErrorMessage                string                            ข้อความ Error
Result                      array   
Result.BucketID             string           1-20             BucketID
Result.ID                   int              1-20
Result.BatchRequestID       string           1-32             Batch bet request number, if it is null, it means single bet
                                                              รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
Result.BetRequestID         string           1-32             Bet Request Number
                                                              รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
Result.BetScore             float            1-16             Bet Amount (Inventory) ค่าต้องไม่เป็น 0
                                                              รูปแบบ: เลขฐาน10 ทศนิยม 4 ตำแหน่ง                                                           
Result.BetStatus            int              1-4              Bet status (please refer to table 10)
Result.BetTime              string           `-`              Bet time
                                                              รูปแบบ: Y-m-d H:i:s
Result.BucketPublicScore    float            1-16             Bucket common point (currently no use)
                                                              รูปแบบ: เลขฐาน10 ทศนิยม 4 ตำแหน่ง  
Result.BucketRebateRate     float            1-16             Bucket return rate (currently no use)
                                                              รูปแบบ: เลขฐาน10 ทศนิยม 4 ตำแหน่ง                                                                                                                      
Result.ClientIP             string           1-50             IP รูปแบบ: IPV4, IPV6
Result.ClientType           int              1-4              Client type ID (1: web version, 2: download version, 3: mobile version)
Result.Currency             string           1-10             สกุลเงิน (please refer to table 11)
Result.CurrentScore         float            1-16             Current bet balance 
                                                              รูปแบบ: เลขฐาน10 ทศนิยม 4 ตำแหน่ง      
Result.DetailURL            string           1-100            แสดงข้อมูลเพิ่มเติมเกี่ยวกับการเดิมพันระบบเกมเช่นเกมฟรีและผลลัพธ์ของการ์ด
Result.ExchangeRate         float            1-16             อัตราแลกเปลี่ยน
                                                              รูปแบบ: เลขฐาน10 ทศนิยม 4 ตำแหน่ง      
Result.FinalScore           float            1-16             ชนะหรือแพ้ WinScore + RebateScore + JackpotScore + RewardScore
                                                              รูปแบบ: เลขฐาน10 ทศนิยม 4 ตำแหน่ง
Result.FundRate             float            1-16             Player Jackpot Provident Fund Ratio (currently unused)
                                                              รูปแบบ: เลขฐาน10 ทศนิยม 4 ตำแหน่ง
Result.GameDepartmentID     string           1-20             Game Pavilion
                                                              รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
Result.GameItem             string           1-20             game item รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
Result.GameType             int              1-4              1. Live game, 2. Lottery, 3. Electronics, 4. Sports, 5. Yes, 6. Finance, 7. Electricity
Result.JackpotScore         float            1-16             Player bonus value
                                                              รูปแบบ: เลขฐาน10 ทศนิยม 4 ตำแหน่ง
Result.Member               string           1-20             Member
Result.MemberID             string           1-20             MemberID
Result.MemberName           string           1-50             ชื่อผู้เล่น
Result.NoRun                string           1-20             
Result.NoActive             string           1-20             
Result.Odds                 float            1-14             Odds รูปแบบ: เลขฐาน10 ทศนิยม 4 ตำแหน่ง
Result.OriginID             int              1-20             Original bet serial number (default is null, it is worth if you change cards)
Result.OriginBetRequestID   string           1-32             Original bet request number (default is null, it is only worth if you change cards)
                                                              รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข  
Result.LastBetRequestID     string           1-32             Place bet request number (default is null, it is worth if you change cards)
                                                              รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข  
Result.PortionRate          float            1-16             0 (not currently used) รูปแบบ: เลขฐาน10 ทศนิยม 4 ตำแหน่ง
Result.ProviderID           string           1-20             ProviderID
Result.PublicScore          float            1-16             PublicScore (currently no use)
Result.RebateRate           float            1-16             Player Rebate (currently no use)
Result.RewardScore          float            1-16             Reward and punishment (positive and negative values), if there is no preset 0
                                                              รูปแบบ: เลขฐาน10 ทศนิยม 4 ตำแหน่ง
Result.ServerID             string           1-100            ServerID
Result.ServerName           string           1-50             ServerName
Result.SettlementTime       string           `-`              Settlement Time รูปแบบ: Y-m-d H:i:s
Result.StakeID              string           1-20             
Result.StakeName            string           1-100            ServerID
Result.SubClientType        string           1-10             (Not currently used)
Result.ValidBetScore        float            1-16             Effective stake (physical amount)
                                                              รูปแบบ: เลขฐาน10 ทศนิยม 4 ตำแหน่ง
Result.WinScore             float            1-16             Winning and losing results (positive and negative values), excluding rebates, winnings and player common points.
                                                              รูปแบบ: เลขฐาน10 ทศนิยม 4 ตำแหน่ง
=========================== ================ ================ =======================================================

Response status code and description
------------------------------------

================ ======================================================= 
ErrorCode        description
================ =======================================================
0                successful
V003             Parameter error
V999             Data format error
V405             Transfer call exception
================ =======================================================

BetStatus
----------

=============== ======================================================= 
Code            description
=============== =======================================================
1               Successful bet
3               Authorities cancel
4               Normal statistics
5               Recount
6               Cancel afterwards
-1              Bet response failed: (An explicit response with an error code.)
-2              No response to bet: (connection timeout, server exception, etc.)
=============== =======================================================

Example Input
-------------
.. code-block:: JSON
    :linenos:

    {
        "RequestID": "9888867789a4551e82f1456110060000",
        "BucketID": "h3RealTHSiteA",
        "MaxID": 105378800,
        "GameType":1,
        "ProviderID":"royal"
    }



Example Output
---------------
.. code-block:: JSON
    :linenos:

    {
        "RequestID": "9888867789a4551e82f1456110060000",
        "ErrorCode": "0",
        "ErrorMessage": "成功",
        "Result": [
            {
                "ID": 105378824,
                "BatchRequestID": "f82ae0164ec848cfb0b0ac05343e2600",
                "BetRequestID": "482E97AF2AFEC2E9BA2300005109D915",
                "OriginID": 0,
                "ProviderID": "Royal",
                "BucketID": "h3RealTHSiteA",
                "MemberID": "testClark",
                "Member": "testClark",
                "MemberName": "Clark測試",
                "ClientType": 3,
                "SubClientType": "s",
                "ClientIP": "22 0.133.245.131",
                "NoRun": "011018005",
                "NoActive": "0215",
                "ServerID": "0604250002",
                "GameItem": "Bacc",
                "ServerName": "BaccA",
                "GameType": 1,
                "GameDepartmentID": "RoyalGclub",
                "StakeID": "06042500020004",
                "StakeName": "BankerPair",
                "Odds": 12,
                "BetScore": 625,
                "ValidBetScore": 625,
                "WinScore": -625,
                "FinalScore": -625,
                "BucketRebateRate": 0,
                "RebateRate": 0,
                "JackpotScore": 0,
                "PortionRate": 0,
                "BucketPublicScore": 0,
                "PublicScore": 0,
                "FundRate": 0,
                "RewardScore": 0,
                "CurrentScore": 25375,
                "BetTime": "2018-10-18 16:21:38",
                "SettlementTime": "2018-10-18 16:22:11",
                "Currency": "THB",
                "ExchangeRate": 0.91,
                "BetStatus": 4,
                "DetailURL": "http://h3-game.rg-show.com/OpenDetail?ServerID=0604250002&NoRun=011018005&NoActive=0215",
                "LastBetRequestID": null,
                "OriginBetRequestID": null
            }
        ]
    }

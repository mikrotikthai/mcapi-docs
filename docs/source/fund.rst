ยอดเงิน (VPSWFundTransfer)
==============================
เติมเงิน ถอนเงิน เรียกดูรายการเติมเงิน

เติมเงิน
***********************
    - version
        X-RG-VER = 1

Input content
-------------

================ ================ ================ ================ ======================================================= 
parameter name   type             length           required         description
================ ================ ================ ================ =======================================================
RequestID        string           32               yes              เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้ง
                                                                    รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
Control          int              `-`              yes              1 เปลี่ยนแปลงยอดเงิน
TransferID       string           1-32             yes              transaction number(กำหนดเอง)
                                                                    รูปแบบ: ตัวอักษรภาษาอังกฤษ และตัวเลข
BucketID         string           1-20             yes              BucketID
MemberID         string           1-20             yes              รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
TransferMoney    string           6-16             yes              จำนวนเงินที่จะเติม และต้องไม่เท่ากับ 0
                                                                    รูปแบบ: ตัวเลขฐานสิบ ทศนิยม 4 ตำแหน่ง
Operator         string           1-20             yes              Operator
IP               string           1-50             yes              IP รูปแบบ: IPV4, IPV6
================ ================ ================ ================ =======================================================

Output content
--------------

===================== ================ ================ ======================================================= 
field                 type             length           description
===================== ================ ================ =======================================================
RequestID             string           32               เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้งค่าจะต้องตรงกับ input ที่ส่งไป
                                                        รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
ErrorCode             string           1-4              สถานะ 0 สำเร็จ อื่นๆ มีข้อผิดพลาด
ErrorMessage          string                            ข้อความ Error
Result                object                            result object
Result.BeforMoney     string           6-16             ยอดเงินก่อนหน้าที่จะเปลี่ยนแปลง
Result.AfterMoney     string           6-16             ยอดเงินหลังจากเปลี่ยนแปลงแล้ว
Result.TransferID     string           1-32             transaction number(กำหนดเอง)
                                                        รูปแบบ: ตัวอักษรภาษาอังกฤษ และตัวเลข
Result.TransferMoney  string           6-16             จำนวนเงินที่จะเติม หรือ ถอน และต้องไม่เท่ากับ 0
                                                        รูปแบบ: ตัวเลขฐานสิบ ทศนิยม 4 ตำแหน่ง
===================== ================ ================ =======================================================

Response status code and description
------------------------------------

================ ======================================================= 
ErrorCode        description
================ =======================================================
0                successful
V001             missing required parameters
V207             request number already exists
V402             account number locked
V407             account number does not exist
V701             existing quota Insufficient to extract
V702             Recharge extraction failed
V708             TransferID already exists

================ =======================================================

Example Input
-------------
.. code-block:: JSON
    :linenos:

    {
        "Control": 1,
        "RequestID": "98884671d4a4461989992d0077777777",
        "TransferID": "tt000001",
        "TransferMoney": "10000.0000",
        "BucketID": "h3RealTHSiteA" ,
        "MemberID": "ffffffff",
        "Operator": "Name",
        "IP": "0.0.0.0"
    }


Example Output
---------------
.. code-block:: JSON
    :linenos:

    {
        "RequestID": "98884671d4a4461989992d0077777777",
        "ErrorCode": "0",
        "ErrorMessage":"Success",
        "Result": {
            "BeforMoney": "0.0000",
            "AfterMoney": "10000.0000",
            "TransferID":"tt000001",
            "TransferMoney":"10000.0000"
        }
    }


ถอนเงิน
***********************
    - version
        X-RG-VER = 1

Input content
-------------

================ ================ ================ ================ ======================================================= 
parameter name   type             length           required         description
================ ================ ================ ================ =======================================================
RequestID        string           32               yes              เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้ง
                                                                    รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
Control          int              `-`              yes              1 เปลี่ยนแปลงยอดเงิน
TransferID       string           1-32             yes              transaction number(กำหนดเอง)
                                                                    รูปแบบ: ตัวอักษรภาษาอังกฤษ และตัวเลข
BucketID         string           1-20             yes              BucketID
MemberID         string           1-20             yes              รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
TransferMoney    string           6-16             yes              จำนวนเงินที่จะถอน เป็นค่าติดลบ และต้องไม่เท่ากับ 0
                                                                    รูปแบบ: ตัวเลขฐานสิบ ทศนิยม 4 ตำแหน่ง
Operator         string           1-20             yes              Operator
IP               string           1-50             yes              IP รูปแบบ: IPV4, IPV6
================ ================ ================ ================ =======================================================

Output content
--------------

===================== ================ ================ ======================================================= 
field                 type             length           description
===================== ================ ================ =======================================================
RequestID             string           32               เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้งค่าจะต้องตรงกับ input ที่ส่งไป
                                                        รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
ErrorCode             string           1-4              สถานะ 0 สำเร็จ อื่นๆ มีข้อผิดพลาด
ErrorMessage          string                            ข้อความ Error
Result                object                            result object
Result.BeforMoney     string           6-16             ยอดเงินก่อนหน้าที่จะเปลี่ยนแปลง
Result.AfterMoney     string           6-16             ยอดเงินหลังจากเปลี่ยนแปลงแล้ว
Result.TransferID     string           1-32             transaction number(กำหนดเอง)
                                                        รูปแบบ: ตัวอักษรภาษาอังกฤษ และตัวเลข
Result.TransferMoney  string           6-16             จำนวนเงินที่จะเติม หรือ ถอน และต้องไม่เท่ากับ 0
                                                        รูปแบบ: ตัวเลขฐานสิบ ทศนิยม 4 ตำแหน่ง
===================== ================ ================ =======================================================

Response status code and description
------------------------------------

================ ======================================================= 
ErrorCode        description
================ =======================================================
0                successful
V001             missing required parameters
V207             request number already exists
V402             account number locked
V407             account number does not exist
V701             existing quota Insufficient to extract
V702             Recharge extraction failed
V708             TransferID already exists

================ =======================================================

Example Input
-------------
.. code-block:: JSON
    :linenos:

    {
        "Control": 1,
        "RequestID": "98884671d4a4461989992d0077777777",
        "TransferID": "tt000002",
        "TransferMoney": "-1000.0000",
        "BucketID": "h3RealTHSiteA" ,
        "MemberID": "ffffffff",
        "Operator": "Name",
        "IP": "0.0.0.0"
    }


Example Output
---------------
.. code-block:: JSON
    :linenos:

    {
        "RequestID": "98884671d4a4461989992d0077777777",
        "ErrorCode": "0",
        "ErrorMessage":"Success",
        "Result": {
            "BeforMoney": "10000.0000",
            "AfterMoney": "9000.0000",
            "TransferID":"tt000002",
            "TransferMoney":"-1000.0000"
        }
    }


ประวัติการเติม-ถอน (by date range)
************************************************

ประวัติการเติม-ถอน ตามช่วงวันที่ 

Input content
-------------

================ ================ ================ ================ ======================================================= 
parameter name   type             length           required         description
================ ================ ================ ================ =======================================================
RequestID        string           32               yes              เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้ง
                                                                    รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
Control          int              `-`              yes              4 query
BucketID         string           1-20             yes              BucketID
MemberID         string           1-20             yes              รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
StartDate        string           `-`              yes              ตัวอย่าง: 2018-01-02 03:00:00
                                                                    รูปแบบ: Y-m-d H:i:s
EndDate          string           `-`              yes              ตัวอย่าง: 2018-01-02 03:00:00
                                                                    รูปแบบ: Y-m-d H:i:s
================ ================ ================ ================ =======================================================

Output content
--------------

=============================== ================ ================ ======================================================= 
field                           type             length           description
=============================== ================ ================ =======================================================
RequestID                       string           32               เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้งค่าจะต้องตรงกับ input ที่ส่งไป
                                                                  รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
ErrorCode                       string           1-4              สถานะ 0 สำเร็จ อื่นๆ มีข้อผิดพลาด
ErrorMessage                    string                            ข้อความ Error
Result                          array                             
Result.GameType                 string           6-16             Game type
Result.BeforMoney               string           6-16             ยอดเงินก่อนหน้าที่จะเปลี่ยนแปลง
Result.AfterMoney               string           6-16             ยอดเงินหลังจากเปลี่ยนแปลงแล้ว
Result.TransferID               string           1-32             transaction number
                                                                  รูปแบบ: ตัวอักษรภาษาอังกฤษ และตัวเลข
Result.TransferMoney            string           6-16             จำนวนเงินที่จะเติม หรือ ถอน และต้องไม่เท่ากับ 0
                                                                  รูปแบบ: ตัวเลขฐานสิบ ทศนิยม 4 ตำแหน่ง
Result.CreateTime               string           `-`              รูปแบบ: Y-m-d H:i:s       
Result.Operator                 string           1-20             Operator
Result.FundTransferRequestID    string           1-32                                                          
=============================== ================ ================ =======================================================

Response status code and description
------------------------------------

================ ======================================================= 
ErrorCode        description
================ =======================================================
0                successful
V001             missing required parameters
V099             Query No data
================ =======================================================

Example Input
-------------
.. code-block:: JSON
    :linenos:

    {
        "Control": 4,
        "RequestID": "98884671d4a4461989992d0900000055",
        "BucketID": "h3RealTHSiteA",
        "MemberID": "ffffffff",
        "StartDate": "2018-10-10 12:00:00",
        "EndDate" : "2018-10-20 12:00:00"
    }



Example Output
---------------
.. code-block:: JSON
    :linenos:

    {
        "RequestID":"98884671d4a4461989992d0900000055",
        "ErrorCode":"0",
        "ErrorMessage":"Success",
        "Result": [
            {
                "TransferID":"tt000001",
                "GameType":"0",
                "TransferMoney":"10000.0000",
                "BeforMoney":"0.0000",
                "AfterMoney":"10000.0000",
                "CreateTime":"2018-10-12 05:41:57.739",
                "Operator": "Name",
                "FundTransferRequestID":"98884671d4a4461989992d0077777777"
            }
        ]
    }


ประวัติการเติม-ถอน (by the RequestID)
************************************************

ประวัติการเติม-ถอน ตาม RequestID

Input content
-------------

======================= ================ ================ ================ ======================================================= 
parameter name          type             length           required         description
======================= ================ ================ ================ =======================================================
RequestID               string           32               yes              เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้ง
                                                                           รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
Control                 int              `-`              yes              5 query
BucketID                string           1-20             yes              BucketID
FundTransferRequestID   string           1-32             yes              RequestID
======================= ================ ================ ================ =======================================================

Output content
--------------

=============================== ================ ================ ======================================================= 
field                           type             length           description
=============================== ================ ================ =======================================================
RequestID                       string           32               เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้งค่าจะต้องตรงกับ input ที่ส่งไป
                                                                  รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
ErrorCode                       string           1-4              สถานะ 0 สำเร็จ อื่นๆ มีข้อผิดพลาด
ErrorMessage                    string                            ข้อความ Error
Result                          array                             
Result.GameType                 string           6-16             Game type
Result.BeforMoney               string           6-16             ยอดเงินก่อนหน้าที่จะเปลี่ยนแปลง
Result.AfterMoney               string           6-16             ยอดเงินหลังจากเปลี่ยนแปลงแล้ว
Result.TransferID               string           1-32             transaction number
                                                                  รูปแบบ: ตัวอักษรภาษาอังกฤษ และตัวเลข
Result.TransferMoney            string           6-16             จำนวนเงินที่จะเติม หรือ ถอน และต้องไม่เท่ากับ 0
                                                                  รูปแบบ: ตัวเลขฐานสิบ ทศนิยม 4 ตำแหน่ง
Result.CreateTime               string           `-`              รูปแบบ: Y-m-d H:i:s       
Result.Operator                 string           1-20             Operator
Result.FundTransferRequestID    string           1-32                                                          
=============================== ================ ================ =======================================================

Response status code and description
------------------------------------

================ ======================================================= 
ErrorCode        description
================ =======================================================
0                successful
V001             missing required parameters
V099             Query No data
================ =======================================================

Example Input
-------------
.. code-block:: JSON
    :linenos:

    {
        "Control": 5,
        "RequestID": "9800000000000000010000aa00000002",
        "BucketID": "h3RealTHSiteA",
        "FundTransferRequestID":"98884671d4a4461989992d0077777777"
    }



Example Output
---------------
.. code-block:: JSON
    :linenos:

    {
        " RequestID ":"9800000000000000010000aa00000002",
        " ErrorCode ":"0",
        " ErrorMessage ":" Success ",
        " Result ": [
            {
                "TransferID":"tt000001",
                "GameType":"0",
                "TransferMoney":"10000.0000",
                "BeforMoney":"0.0000",
                "AfterMoney":"10000.0000",
                "CreateTime":"2018-10-12 05:41:57.739",
                "Operator": "Name",
                "FundTransferRequestID":"98884671d4a4461989992d0077777777"
            }
        ]
    }

ประวัติการเติม-ถอน (by the TransferID)
************************************************

ประวัติการเติม-ถอน ตาม TransferID

Input content
-------------

======================= ================ ================ ================ ======================================================= 
parameter name          type             length           required         description
======================= ================ ================ ================ =======================================================
RequestID               string           32               yes              เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้ง
                                                                           รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
Control                 int              `-`              yes              6 query
BucketID                string           1-20             yes              BucketID
TransferID              string           1-32             yes              TransferID
======================= ================ ================ ================ =======================================================

Output content
--------------

=============================== ================ ================ ======================================================= 
field                           type             length           description
=============================== ================ ================ =======================================================
RequestID                       string           32               เป็นค่าอ้างอิงสำหรับการส่งข้อมูลในแต่ละครั้งค่าจะต้องตรงกับ input ที่ส่งไป
                                                                  รูปแบบ: อันเดอร์สกอร์, เครืองหมายลบ, ตัวอักษรภาษาอังกฤษ และตัวเลข
ErrorCode                       string           1-4              สถานะ 0 สำเร็จ อื่นๆ มีข้อผิดพลาด
ErrorMessage                    string                            ข้อความ Error
Result                          array                             
Result.GameType                 string           6-16             Game type
Result.BeforMoney               string           6-16             ยอดเงินก่อนหน้าที่จะเปลี่ยนแปลง
Result.AfterMoney               string           6-16             ยอดเงินหลังจากเปลี่ยนแปลงแล้ว
Result.TransferID               string           1-32             transaction number
                                                                  รูปแบบ: ตัวอักษรภาษาอังกฤษ และตัวเลข
Result.TransferMoney            string           6-16             จำนวนเงินที่จะเติม หรือ ถอน และต้องไม่เท่ากับ 0
                                                                  รูปแบบ: ตัวเลขฐานสิบ ทศนิยม 4 ตำแหน่ง
Result.CreateTime               string           `-`              รูปแบบ: Y-m-d H:i:s       
Result.Operator                 string           1-20             Operator
Result.FundTransferRequestID    string           1-32                                                          
=============================== ================ ================ =======================================================

Response status code and description
------------------------------------

================ ======================================================= 
ErrorCode        description
================ =======================================================
0                successful
V001             missing required parameters
V099             Query No data
================ =======================================================

Example Input
-------------
.. code-block:: JSON
    :linenos:

    {
        "Control": 6,
        "RequestID": "9800000000000000010000aa00000002",
        "BucketID": "h3RealTHSiteA",
        "TransferID":"tt000001"
    }



Example Output
---------------
.. code-block:: JSON
    :linenos:

    {
        " RequestID ":"9800000000000000010000aa00000002",
        " ErrorCode ":"0",
        " ErrorMessage ":" Success ",
        " Result ": [
            {
                "TransferID":"tt000001",
                "GameType":"0",
                "TransferMoney":"10000.0000",
                "BeforMoney":"0.0000",
                "AfterMoney":"10000.0000",
                "CreateTime":"2018-10-12 05:41:57.739",
                "Operator": "Name",
                "FundTransferRequestID":"98884671d4a4461989992d0077777777"
            }
        ]
    }